package ru.tsc.tambovtsev.tm;

import ru.tsc.tambovtsev.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}