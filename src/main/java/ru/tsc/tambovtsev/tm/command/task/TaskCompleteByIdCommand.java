package ru.tsc.tambovtsev.tm.command.task;

import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-complete-by-id";

    public static final String DESCRIPTION = "Complete task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().changeTaskStatusById(id, Status.COMPLETED);
        task.setDateEnd(new Date());
    }

}
