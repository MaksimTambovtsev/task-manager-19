package ru.tsc.tambovtsev.tm.command;

import ru.tsc.tambovtsev.tm.api.model.ICommand;
import ru.tsc.tambovtsev.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    public void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

}
