package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

   // User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

}
