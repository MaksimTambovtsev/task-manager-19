package ru.tsc.tambovtsev.tm.api.model;

import ru.tsc.tambovtsev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
