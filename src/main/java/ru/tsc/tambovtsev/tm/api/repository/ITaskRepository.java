package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    Task create(String name);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

}