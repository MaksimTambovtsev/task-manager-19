package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractEntity> {

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    M add(M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}
