package ru.tsc.tambovtsev.tm.api.repository;

import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}