package ru.tsc.tambovtsev.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    String id = UUID.randomUUID().toString();

    public String getId() {return id;}

    public void setId(String id) { this.id = id;}

}
