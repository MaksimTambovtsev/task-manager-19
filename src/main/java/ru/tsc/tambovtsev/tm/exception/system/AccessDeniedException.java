package ru.tsc.tambovtsev.tm.exception.system;

import ru.tsc.tambovtsev.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
