package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.api.service.IService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.IndexIncorrectException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;
import ru.tsc.tambovtsev.tm.repository.AbstractRepository;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractEntity, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    public M add(M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M remove(M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }
}
