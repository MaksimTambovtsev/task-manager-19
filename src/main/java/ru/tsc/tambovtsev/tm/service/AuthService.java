package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.service.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.LoginEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.PasswordEmptyException;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;
import ru.tsc.tambovtsev.tm.util.HashUtil;


public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public void logout() {
        if (isAuth()) throw new AccessDeniedException();
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public String getUserId() {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}
